document.addEventListener("DOMContentLoaded", function () {
  let slides = document.querySelectorAll(".slides");
  let prevBtns = document.querySelectorAll(".prev-btn");
  let prevBtnsforMobile = document.querySelector(".desktopNone .prev-btn");
  // let nextBtnsforMobile = document.querySelector(".desktopNone .nxt-btn");
  // let serviceInfos = document.querySelectorAll(".service-info");
  // let checkboxes = document.querySelectorAll(
  //   '.service-checkbox input[type="checkbox"]'
  // );
  // console.log(checkboxes)
  // console.log("servieInfos - ", serviceInfos)
  let buttonsAllContainer = document.querySelectorAll(".buttons"); // down container [prev] , [next] steps
  // console.log(buttonsAllContainer)
  let nameInput = document.querySelector(".nameInput");
  let emailInput = document.querySelector(".emailInput");
  let numberInput = document.querySelector(".numberInput");
  let desktopNone = document.querySelector(".desktopNone");
  let numberDirections = document.querySelectorAll(".directions .number");
  let checkboxPlans = document.querySelector(".checkboxPlans");
  let personalDetails = {};

  let planContainerSlide2 = document.querySelector(".planContainerSlide2");
  let finalSelectedPlan = document.querySelector(".finalSelectedPlan");
  let planFinalised = document.querySelector(".planFinalised");
  console.log(planFinalised.innerHTML);
  let currentSelected = "";
  planContainerSlide2.addEventListener("click", (event) => {
    if (event.target.tagName == "INPUT") {
      let addIngBorder = document.querySelector(`.${event.target.value}`);
      // let currentPlan = document.querySelector(
      //   `.${event.target.value} .plan-cost`
      // );

      currentSelected = event.target.value;
      let finalSelectedPlan = document.querySelector(".finalSelectedPlan");
      finalSelectedPlan.classList.remove("finalSelectedPlan");
      addIngBorder.classList.add("finalSelectedPlan");
      planSelected.textContent = document.querySelector(
        ".finalSelectedPlan .plan-cost"
      ).textContent;
      console.log("addIngBorder ", addIngBorder);
      // advanced arcade pro
      let value = "";
      if (addIngBorder.classList.contains("arcade")) {
        value = "Arcade ";
      } else if (addIngBorder.classList.contains("pro")) {
        value = "Pro ";
      } else {
        value = "Advanced ";
      }
      if (checkboxPlans.checked == true) {
        value += "(Monthly)";
      } else {
        value += "(Yearly)";
      }
      planFinalised.innerHTML = value;
    }
  });

  let monthlyPlan = document.querySelector(".monthlyPlan");
  let yearlyPlan = document.querySelector(".yearlyPlan");

  checkboxPlans.style.display = "none";
  let allPlanCost = document.querySelectorAll(".plan-cost");
  let benefits = document.querySelectorAll(".benefits");
  let boxBorderBefore = document.querySelector(".boxBorder");

  let allServiceCosts = document.querySelectorAll(".service-cost p"); /// this is for select your plan
  // console.log( allServiceCosts )
  let finishingUpCosts = document.querySelectorAll(".div2-para2  .planCost");
  let planSelected = document.querySelector(".planSelected");
  let seasonPara1 = document.querySelector(".season-para1");
  let seasonPara2 = document.querySelector(".season-para2");

  checkboxPlans.addEventListener("click", () => {
    let displayPLan = document.querySelector(".finalSelectedPlan");

    if (displayPLan.classList.contains("arcade")) {
      value = "Arcade ";
    } else if (displayPLan.classList.contains("pro")) {
      value = "Pro ";
    } else {
      value = "Advanced ";
    }
    if (checkboxPlans.checked == true) {
      seasonPara1.textContent = "Total (per month)";
      value += `(Monthly)`;
      planFinalised.innerHTML = value;
      // let bill = seasonPara2.textContent;

      monthlyPlan.style.color = "hsl(213, 96%, 18%)";
      yearlyPlan.style.color = "hsl(231, 11%, 63%)";
      benefits.forEach((item) => {
        item.classList.add("none");
      });
      allPlanCost.forEach((plan, index) => {
        let addZero = plan.textContent;
        let answer = addZero.split("0");
        answer.join("");
        plan.textContent = answer[0] + "/mo";
      });

      allServiceCosts.forEach((service, index) => {
        let addZero = service.textContent;
        let answer = addZero.split("0");
        answer.join("");
        service.textContent = answer[0] + "/mo";
      });
      finishingUpCosts.forEach((service) => {
        let addZero = service.textContent;
        let answer = addZero.split("0");
        answer.join("");
        service.textContent = answer[0] + "/mo";
      });

      planSelected.textContent = document.querySelector(
        ".finalSelectedPlan .plan-cost"
      ).textContent;
    } else {
      value += `(Yearly)`;
      planFinalised.innerHTML = value;
      seasonPara1.textContent = "Total (per year)";
      monthlyPlan.style.color = "hsl(231, 11%, 63%)";
      yearlyPlan.style.color = "hsl(213, 96%, 18%)";

      benefits.forEach((item) => {
        item.classList.remove("none");
      });
      allPlanCost.forEach((plan, index) => {
        let addZero = plan.textContent;
        let answer = addZero.split("/");
        answer[0] += "0/yr";
        plan.textContent = answer[0];
      });
      allServiceCosts.forEach((service, index) => {
        let addZero = service.textContent;
        let answer = addZero.split("/");
        answer[0] += "0/yr";
        service.textContent = answer[0];
      });
      finishingUpCosts.forEach((service) => {
        let addZero = service.textContent;
        let answer = addZero.split("/");
        answer[0] += "0/yr";
        service.textContent = answer[0];
      });
      planSelected.textContent = document.querySelector(
        ".finalSelectedPlan .plan-cost"
      ).textContent;
    }
  });

  // let onlineServiceCost = document.querySelector(".onlineServiceCost");
  // let largerStorageCost = document.querySelector('.largerStorageCost');
  // let CustomProfitCost = document.querySelector('.CustomProfitCost');

  let choosenOnline = document.querySelector(".choosenOnline");
  let choosenExtraStorage = document.querySelector(".choosenExtraStorage");
  let ChoosenProfit = document.querySelector(".ChoosenProfit");

  let checkBoxes = document.querySelector(".check-boxes");

  let CustomProfitButton = document.querySelector("#CustomProfit");
  let largerStorageButton = document.querySelector("#largerStorage");
  let onlineServiceButton = document.querySelector("#onlineService");

  // PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS PICK ADD ONS

  let cashTotal = 0;
  checkBoxes.addEventListener("click", (event) => {
    if (event.target.tagName == "INPUT") {
      // console.log(
      //   event.target.id,
      //   event.target.tagName,
      //   event.target.parentElement.parentElement.innerHTML
      // );
      // console.log(
      //   " event.target.parentElement.parentElement.classLIST",
      //   event.target.parentElement.parentElement.classList
      // );
      if (event.target.id == "CustomProfit") {
        if (CustomProfitButton.checked) {
          ChoosenProfit.classList.add("space-between");
          ChoosenProfit.classList.remove("none");
          event.target.parentElement.parentElement.classList.add(
            "serviceCheck"
          );
        } else {
          ChoosenProfit.classList.add("none");
          ChoosenProfit.classList.remove("space-between");
          event.target.parentElement.parentElement.classList.remove(
            "serviceCheck"
          );
        }
      } else if (event.target.id == "largerStorage") {
        if (largerStorageButton.checked) {
          choosenExtraStorage.classList.add("space-between");
          choosenExtraStorage.classList.remove("none");
          event.target.parentElement.parentElement.classList.add(
            "serviceCheck"
          );
        } else {
          choosenExtraStorage.classList.add("none");
          choosenExtraStorage.classList.remove("space-between");
          event.target.parentElement.parentElement.classList.remove(
            "serviceCheck"
          );
        }
      } else {
        if (onlineServiceButton.checked) {
          choosenOnline.classList.add("space-between");
          choosenOnline.classList.remove("none");
          event.target.parentElement.parentElement.classList.add(
            "serviceCheck"
          );
        } else {
          choosenOnline.classList.add("none");
          choosenOnline.classList.remove("space-between");
          event.target.parentElement.parentElement.classList.remove(
            "serviceCheck"
          );
        }
      }
    }
  });
  let changePlan = document.querySelector(".changePlan");
  changePlan.addEventListener("click", () => {
    console.log("clicked");
    slides.forEach((slide, index) => {
      if (index == 1) {
        slide.classList.remove("none");
      } else {
        if (!slide.classList.contains("none")) {
          slide.classList.add("none");
        }
      }
    });
  });

  let count = 0;
  if (count == 0) {
    prevBtns[0].disabled = true;
    prevBtnsforMobile.style.display = "none";
    desktopNone.style.justifyContent = "flex-end";
  }
  // prev , next button
  buttonsAllContainer.forEach((buttonsContainer, index) => {
    buttonsContainer.addEventListener("click", (event) => {
      if (event.target.classList.contains("nxt-btn")) {
        nxtFunc(index);
        stepsInfoBacnkgroundColor(index + 1);
      }
      if (event.target.classList.contains("prev-btn")) {
        prevFunc(index);
        stepsInfoBacnkgroundColor(index - 1);
      }
    });
  });
  desktopNone.addEventListener("click", (event) => {
    // console.log(event.target);
    let currentIndex = "";
    slides.forEach((slide, index) => {
      if (!slide.classList.contains("none")) {
        currentIndex = Number(index);
      }
    });
    let process = true;
    if (currentIndex == 0) {
      if (slide1Finalcheck()) {
        process = true;
      } else {
        process = false;
      }
    }
    if (process) {
      if (event.target.classList.contains("nxt-btn")) {
        nxtFunc(currentIndex);
        stepsInfoBacnkgroundColor(currentIndex + 1);
        prevBtnsforMobile.style.display = "flex";
        desktopNone.style.justifyContent = "space-between";
        desktopNone.style.alignItems = "center";
        if (currentIndex == 3) {
          desktopNone.style.display = "none";
        }
      }
      if (event.target.classList.contains("prev-btn")) {
        prevFunc(currentIndex);
        stepsInfoBacnkgroundColor(currentIndex - 1);
      }
    }
  });

  const stepsInfoBacnkgroundColor = (index) => {
    let lightblueCircle = document.querySelector(".light-blue-backg");
    if (index < 4 && index >= 0) {
      lightblueCircle.classList.remove("light-blue-backg");
      numberDirections[index].classList.add("light-blue-backg");
    }
  };

  function prevFunc(count) {
    if (count > 0) {
      slides[count].classList.add("none");
      count -= 1;
      slides[count].classList.remove("none");
      prevBtns[count].disabled = false;
      if (count == 0) {
        prevBtnsforMobile.style.display = "none";
        desktopNone.style.justifyContent = "flex-end";
      }
    }
  }

  function nxtFunc(count) {
    let continueProcess = false;
    if (count == 0) {
      continueProcess = slide1Finalcheck();
    } else if (count == 2) {
      makeBill();
      continueProcess = true;
    } else if (count < slides.length - 1) {
      continueProcess = true;
    } else {
      continueProcess = false;
    }

    if (continueProcess) {
      slides[count].classList.add("none");
      count += 1;
      if (count == slides.length - 1) {
        console.log("maximum last slide");
        slides[count].classList.remove("none");
      } else {
        slides[count].classList.remove("none");
      }
    }
  }

  const validateIt = () => {
    nameInput.addEventListener("change", () => {
      personalDetails["name"] = nameInput.value;
      personalDetails.name = nameInput.value;
    });
    emailInput.addEventListener("change", () => {
      personalDetails["email"] = emailInput.value;
    });
    numberInput.addEventListener("change", () => {
      personalDetails["number"] = numberInput.value;
    });
  };
  validateIt();

  // slide1Finalcheck   slide1Finalcheck   slide1Finalcheck   slide1Finalcheck   slide1Finalcheck
  function slide1Finalcheck() {
    let statement = true;
    let regexMail = /[a-z0-9]+[@][a-z]+[\.][a-z]{3}/;
    // let regexNumber = /[\+][91]+[1-9]{9}[0-9]{1}/;
    //
    let regexNumber = /[\+91]?[\s]?[1-9][0-9]{9}/;
    if (!personalDetails.name || personalDetails.name.length === 0) {
      let name = document.querySelector(".name");
      name.textContent = `the field is required`;
      name.style.color = "red";
      statement = false;
    } else {
      let name = document.querySelector(".name");
      name.textContent = ``;
    }
    // !personalDetails.email   &&   personalDetails.email.length === 0
    // !personalDetails.number || personalDetails.number.length === 0
    // console.log(regexMail.test(personalDetails.email))
    console.log(
      "regexNumber.test(personalDetails.number) ",
      regexNumber.test(personalDetails.number)
    );
    // console.log( ('personalDetails' , personalDetails ))
    if (!regexMail.test(personalDetails.email)) {
      let email = document.querySelector(".email");
      email.textContent = `the field is required`;
      email.style.color = "red";
      statement = false;
    } else {
      let email = document.querySelector(".email");
      email.textContent = "";
    }
    // !personalDetails.number || personalDetails.number.length === 0
    if (!regexNumber.test(personalDetails.number)) {
      let number = document.querySelector(".numberr");
      number.textContent = `the field is required`;
      number.style.color = "red";
      statement = false;
    } else {
      let number = document.querySelector(".numberr");
      number.textContent = "";
    }
    if (!statement) {
      return false;
    } else {
      return true;
    }
  }

  // SUMMARY    SUMMARY    SUMMARY    SUMMARY     SUMMARY   SUMMARY     SUMMARY    SUMMARY     SUMMARY     SUMMARY
  function makeBill() {
    let servicesChoosed = document.querySelectorAll(".div2-para2 .planCost");
    let totalBill = 0;
    servicesChoosed.forEach((ele) => {
      if (!ele.parentElement.classList.contains("none")) {
        totalBill += Number(ele.textContent.replace(/[^\d]/g, ""));
      }
    });
    let mainPlan = Number(
      document.querySelector(".planSelected").textContent.replace(/[^\d]/g, "")
    );
    totalBill += mainPlan;
    let finalBill = document.querySelector(".finalBill");
    if (checkboxPlans.checked) {
      let answer = `+$${totalBill}/mo`;
      finalBill.textContent = answer;
    } else {
      let answer = `+$${totalBill}/yr`;
      finalBill.textContent = answer;
    }
  }

  // console.clear();
});

////////  MY EXTRA FUTURE  STUFF    /////////

// NEXT FUNCTION
// else if (count == 1) {
// console.log("in next func count 2")
// let answer = slide2Finalcheck()
// console.log(slide1Finalcheck)
// while(!slide2Finalcheck()){
//     slide2Finalcheck()
//     continueProcess = false;
// }
// }

// console.log(addIngBorder)

////////////////

// prevBtns.forEach((btn, index) => {
//   btn.addEventListener("click", () => {
//     prevFunc(index);
//   });
// });

// nxtBtns.forEach((btn, index) => {
//   btn.addEventListener("click", () => {
//     nxtFunc(index);
//   });
// });

////////////////////////

// console.log(onlineServiceCost.textContent.replace(/[^\d]/g , '' ))
// console.log(largerStorageCost.textContent.replace(/[^\d]/g , '' ))
// console.log(CustomProfitCost.textContent.replace(/[^\d]/g , '' ))
